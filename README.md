Henfield Storage is family-owned and operates as part of a successful business group which has been running for over 40 years. All of our personal and business customers benefit from low cost storage at our bases in North & West London, South & Southwest London, Central London, Surrey and, Sussex.

Address: 237 Record Street, Southwark, London SE15 1TL, UK

Phone: +44 20 7277 6391
